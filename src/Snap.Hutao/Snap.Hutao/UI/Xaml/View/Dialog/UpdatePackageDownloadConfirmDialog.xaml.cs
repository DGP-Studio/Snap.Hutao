﻿// Copyright (c) DGP Studio. All rights reserved.
// Licensed under the MIT license.

using Microsoft.UI.Xaml.Controls;

namespace Snap.Hutao.UI.Xaml.View.Dialog;

internal sealed partial class UpdatePackageDownloadConfirmDialog : ContentDialog
{
    public UpdatePackageDownloadConfirmDialog()
    {
        InitializeComponent();
    }
}